package ir.zinutech.android.data.utils

import ir.zinutech.android.data.entities.DeliveryData
import ir.zinutech.android.data.entities.LocationData

object DataTestUtils {


  fun generateDeliveryDataList(): List<DeliveryData> = (0..5).map {
    getTestDeliveryData(it)
  }

  private fun getTestDeliveryData(id: Int): DeliveryData =
      DeliveryData(id, "description_$id", "imageUrl_$id", LocationData(1.0, 2.0, "Address"))


}