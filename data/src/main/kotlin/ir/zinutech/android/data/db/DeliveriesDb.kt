package ir.zinutech.android.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ir.zinutech.android.data.entities.DeliveryData

@Database(
    entities = [DeliveryData::class],
    version = 1,
    exportSchema = false
)
abstract class DeliveriesDb : RoomDatabase() {
  companion object {
    fun create(context: Context): DeliveriesDb {
      return Room.databaseBuilder(context, DeliveriesDb::class.java, "deliveries.db")
          .fallbackToDestructiveMigration()
          .build()
    }
  }

  abstract fun deliveryDao(): DeliveriesDao

}