package ir.zinutech.android.data.mappers

import ir.zinutech.android.data.entities.DeliveryData
import ir.zinutech.android.domain.common.Mapper
import ir.zinutech.android.domain.entities.DeliveryEntity
import ir.zinutech.android.domain.entities.LocationEntity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DeliveryDataEntityMapper @Inject constructor() : Mapper<DeliveryData, DeliveryEntity>() {
  override fun mapFrom(from: DeliveryData): DeliveryEntity {
    return DeliveryEntity(
        from.id,
        from.description,
        from.imageUrl,
        LocationEntity(
            from.location.lat,
            from.location.lng,
            from.location.address
        )
    )
  }
}