package ir.zinutech.android.data.repositories

import io.reactivex.Single
import ir.zinutech.android.domain.DeliveryRepository
import ir.zinutech.android.domain.entities.DeliveryEntity

class DeliveryRepositoryImpl(
    private val deliveryDataStore: DeliveryDataStore) : DeliveryRepository {
  override fun getDeliveryList(
      pageIndex: Int): Single<List<DeliveryEntity>> = deliveryDataStore.getDeliveryList(pageIndex)
}