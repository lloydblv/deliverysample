package ir.zinutech.android.data.api

import io.reactivex.Single
import ir.zinutech.android.data.entities.DeliveryData
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
  @GET("deliveries")
  fun getDeliveries(
      @Query("offset") offset: Int, @Query("limit") limit: Int): Single<List<DeliveryData>>

}