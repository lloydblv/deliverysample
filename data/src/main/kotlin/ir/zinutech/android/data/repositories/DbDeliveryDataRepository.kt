package ir.zinutech.android.data.repositories

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.zinutech.android.data.api.Api
import ir.zinutech.android.data.db.DeliveriesDb
import ir.zinutech.android.data.entities.DeliveryData
import ir.zinutech.android.data.entities.Listing
import ir.zinutech.android.domain.entities.NetworkState
import ir.zinutech.android.data.mappers.DeliveryDataEntityMapper
import ir.zinutech.android.domain.entities.DeliveryEntity
import timber.log.Timber
import java.util.concurrent.Executor

class DbDeliveryDataRepository(
    val db: DeliveriesDb,
    private val api: Api,
    private val compositeDisposable: CompositeDisposable,
    private val ioExecutor: Executor,
    private val networkPageSize: Int = DEFAULT_NETWORK_PAGE_SIZE,
    private val deliveryDataEntityMapper: DeliveryDataEntityMapper) : DeliveryRepository {

  companion object {
    private const val DEFAULT_NETWORK_PAGE_SIZE = 20
  }

  /**
   * Inserts the response into the database while also assigning position indices to items.
   */
  private fun insertResultIntoDb(deliveries: List<DeliveryData>) {
    db.runInTransaction {
      val start = db.deliveryDao().getNextIndexInDeliveries()
      Timber.d("insertResultIntoDb(), deliveries.size:[%s], start:[%s]", deliveries.size, start)

      val items = deliveries.mapIndexed { index, child ->
        child.indexInResponse = start + index
        child
      }
      db.deliveryDao().insert(items)
    }
  }

  /**
   * When refresh is called, we simply run a fresh network request and when it arrives, clear
   * the database table and insert all new items in a transaction.
   * <p>
   * Since the PagedList already uses a database bound data source, it will automatically be
   * updated after the database transaction is finished.
   */
  @MainThread
  private fun refresh(): LiveData<NetworkState> {
    Timber.d("refresh()")
    val networkState = MutableLiveData<NetworkState>()
    networkState.value = NetworkState.LOADING
    compositeDisposable.add(
        api.getDeliveries(0, networkPageSize)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({
              db.runInTransaction {
                db.deliveryDao().deleteDeliveries()
                insertResultIntoDb(it)
              }
              // since we are in bg thread now, post the result.
              networkState.postValue(NetworkState.LOADED)
        }, {
          networkState.value = NetworkState.error(it.message)
        })
    )
    return networkState
  }

  @MainThread
  override fun deliveryList(pageSize: Int): Listing<DeliveryEntity> {
    // create a boundary callback which will observe when the user reaches to the edges of
    // the list and update the database with extra data.
    Timber.d("deliveryList(), pageSize:[%s]", pageSize)

    // create a data source factory from Room
    val dataSourceFactory = db.deliveryDao().deliveryList().map { deliveryDataEntityMapper.mapFrom(it) }
    val boundaryCallback = DeliveryBoundaryCallback(
        ioExecutor = ioExecutor,
        compositeDisposable = compositeDisposable,
        handleResponse = this::insertResultIntoDb,
        networkPageSize = networkPageSize,
        api = api
    )

    val builder = LivePagedListBuilder(dataSourceFactory,
        PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setPrefetchDistance(5)
            .build())
        .setBoundaryCallback(boundaryCallback)

    // we are using a mutable live data to trigger refresh requests which eventually calls
    // refresh method and gets a new live data. Each refresh request by the user becomes a newly
    // dispatched data in refreshTrigger
    val refreshTrigger = MutableLiveData<Unit>()
    val refreshState = Transformations.switchMap(refreshTrigger) {
      refresh()
    }

    return Listing(
        pagedList = builder.build(),
        networkState = boundaryCallback.networkState,
        retry = {
          boundaryCallback.helper.retryAllFailed()
        },
        refresh = {
          refreshTrigger.value = null
        },
        refreshState = refreshState
    )
  }
}