package ir.zinutech.android.data.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ir.zinutech.android.data.entities.DeliveryData


@Dao
interface DeliveriesDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(deliveries : List<DeliveryData>)

  @Query("SELECT * FROM deliveries ORDER BY indexInResponse ASC")
  fun deliveryList() : DataSource.Factory<Int, DeliveryData>

  @Query("DELETE FROM deliveries")
  fun deleteDeliveries()

  @Query("SELECT MAX(indexInResponse) + 1 FROM deliveries")
  fun getNextIndexInDeliveries() : Int
}