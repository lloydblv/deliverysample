package ir.zinutech.android.data.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName



@Entity(tableName = "deliveries")
data class DeliveryData(
    @SerializedName("id")
    @PrimaryKey
    val id: Int,

    @SerializedName("description")
    var description: String,


    @SerializedName("imageUrl")
    @ColumnInfo(name = "image_url")
    var imageUrl: String,

    @SerializedName("location")
    @Embedded
    var location: LocationData
){
    var indexInResponse: Int = -1
}