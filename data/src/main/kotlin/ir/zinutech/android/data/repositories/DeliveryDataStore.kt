package ir.zinutech.android.data.repositories

import io.reactivex.Single
import ir.zinutech.android.data.api.Api
import ir.zinutech.android.data.mappers.DeliveryDataEntityMapper
import ir.zinutech.android.domain.DeliveryDataStore
import ir.zinutech.android.domain.entities.DeliveryEntity

class DeliveryDataStore(private val api: Api) : DeliveryDataStore {

  companion object {
    private const val LIMIT = 20
  }

  private val deliveryDataEntityMapper = DeliveryDataEntityMapper()


  override fun getDeliveryList(pageIndex: Int): Single<List<DeliveryEntity>> {
    return api.getDeliveries(pageIndex, LIMIT).flatMap {
      deliveryDataEntityMapper.single(it)
    }
  }

}