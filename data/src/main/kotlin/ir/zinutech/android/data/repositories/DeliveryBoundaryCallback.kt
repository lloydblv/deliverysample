package ir.zinutech.android.data.repositories

import android.arch.paging.PagingRequestHelper
import android.arch.paging.PagingRequestHelper.RequestType.INITIAL
import androidx.annotation.MainThread
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.zinutech.android.data.api.Api
import ir.zinutech.android.data.entities.DeliveryData
import ir.zinutech.android.data.utils.createStatusLiveData
import ir.zinutech.android.domain.entities.DeliveryEntity
import timber.log.Timber
import java.util.concurrent.Executor

class DeliveryBoundaryCallback(
    val compositeDisposable: CompositeDisposable,
    val api: Api,
    private val handleResponse: (List<DeliveryData>) -> Unit,
    ioExecutor: Executor,
    private val networkPageSize: Int
) : PagedList.BoundaryCallback<DeliveryEntity>() {

  val helper = PagingRequestHelper(ioExecutor)
  val networkState = helper.createStatusLiveData()


  @MainThread
  override fun onZeroItemsLoaded() {
    Timber.d("onZeroItemsLoaded")
    helper.runIfNotRunning(INITIAL) { requestHelperCallback ->
      fetchAndStoreDeliveries(requestHelperCallback, offset = 0)
    }
  }


  private fun fetchAndStoreDeliveries(requestHelperCallback: PagingRequestHelper.Request.Callback,
      offset: Int) {
    Timber.d("fetchAndStoreDeliveries(), requestHelperCallback:[%s], offset:[%s]",
        requestHelperCallback, offset)

    api.getDeliveries(offset = offset, limit = networkPageSize)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribe({
          insertItemsIntoDb(it, requestHelperCallback)
        }, {
          requestHelperCallback.recordFailure(it)
          Timber.e(it, "While fetchAndStoreDeliveries")
        }).also {
          compositeDisposable.add(it)
        }
  }

  /**
   * User reached to the end of the list.
   */
  @MainThread
  override fun onItemAtEndLoaded(itemAtEnd: DeliveryEntity) {
    Timber.d("onItemAtEndLoaded(), itemAtEnd:[%s]", itemAtEnd.id)
    helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER) { requestHelperCallback ->
      fetchAndStoreDeliveries(requestHelperCallback, itemAtEnd.id+1)
    }
  }

  private fun insertItemsIntoDb(deliveries: List<DeliveryData>,
      requestHelperCallback: PagingRequestHelper.Request.Callback) {
    Timber.d("insertItemsIntoDb(), deliveries.size():[%s]", deliveries.size)
    handleResponse.invoke(deliveries)
    requestHelperCallback.recordSuccess()
  }

}