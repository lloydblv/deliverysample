package ir.zinutech.android.data

import io.reactivex.Single
import ir.zinutech.android.data.api.Api
import ir.zinutech.android.data.entities.DeliveryData
import ir.zinutech.android.data.repositories.DeliveryDataStore
import ir.zinutech.android.data.utils.DataTestUtils
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito


class DeliveryDataStoreTests {

  private lateinit var api: Api
  private lateinit var deliveryDataStore: DeliveryDataStore

  @Before
  fun before() {
    api = Mockito.mock(Api::class.java)
    deliveryDataStore = DeliveryDataStore(api)
  }

  @Test
  fun testWhenRequestingTopTracksFromRemoteReturnExpectedResult() {
    val testDeliveryList: List<DeliveryData> = DataTestUtils.generateDeliveryDataList()
    Mockito.`when`(api.getDeliveries(1, 20)).thenReturn(
        Single.just(testDeliveryList))

    deliveryDataStore.getDeliveryList(pageIndex = 1).test()
        .assertValue { it.size == 6 && it[0].id == 0 }
        .assertComplete()
  }

}