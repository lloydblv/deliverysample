package com.company.android.sample.di.components

import com.company.android.sample.di.modules.ContextModule
import com.company.android.sample.di.modules.DataModule
import com.company.android.sample.di.modules.NetModule
import com.company.android.sample.deliveries.injection.DeliveryListModule
import com.company.android.sample.deliveries.injection.DeliveryListSubComponent
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [
  (ContextModule::class),
  (NetModule::class),
  (DataModule::class)])
interface MainComponent {
  fun plus(deliveryListModule: DeliveryListModule): DeliveryListSubComponent
}