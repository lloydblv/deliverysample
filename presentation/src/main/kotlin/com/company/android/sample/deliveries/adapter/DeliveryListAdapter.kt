package com.company.android.sample.deliveries.adapter

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.company.android.sample.R
import com.company.android.sample.commons.RecyclerViewClickListener
import ir.zinutech.android.domain.entities.NetworkState
import ir.zinutech.android.domain.entities.DeliveryEntity

class DeliveryListAdapter(private val requestManager: RequestManager,
    private val retryCallback: () -> Unit,
    private val onListItemClickListener: RecyclerViewClickListener) : PagedListAdapter<DeliveryEntity, RecyclerView.ViewHolder>(
    DeliveryDataDiffCallback) {

  private var networkState: NetworkState? = null

  private var showLoadingMore = false

  companion object {
//    private const val DELIVERY_VIEW_TYPE = 11

    val DeliveryDataDiffCallback = object : DiffUtil.ItemCallback<DeliveryEntity>() {
      override fun areItemsTheSame(oldItem: DeliveryEntity, newItem: DeliveryEntity): Boolean =
          oldItem.id == newItem.id

      override fun areContentsTheSame(oldItem: DeliveryEntity, newItem: DeliveryEntity): Boolean =
          oldItem == newItem
    }
  }

  fun getItem(itemView: View, recyclerView: RecyclerView?): DeliveryEntity? {
    return recyclerView?.getChildAdapterPosition(itemView)?.let {
      getItem(it)
    }
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    when (getItemViewType(position)) {
      R.layout.item_delivery_layout -> (holder as DeliveryViewHolder).bind(getItem(position))
      R.layout.network_state_item -> (holder as NetworkStateItemViewHolder).bindTo(
          networkState)
    }
  }

  override fun onBindViewHolder(
      holder: RecyclerView.ViewHolder,
      position: Int,
      payloads: MutableList<Any>) {
    if (payloads.isNotEmpty()) {
      val item = getItem(position)
      (holder as DeliveryViewHolder).updateScore(item)
    } else {
      onBindViewHolder(holder, position)
    }
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    return when (viewType) {
      R.layout.item_delivery_layout -> DeliveryViewHolder.create(parent, requestManager).apply {
        itemView.setOnClickListener {
          onListItemClickListener.invoke(it, viewType)
        }
      }
      R.layout.network_state_item -> NetworkStateItemViewHolder.create(parent, retryCallback)
      else -> throw IllegalArgumentException("unknown view type $viewType")
    }
  }


  private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED


  override fun getItemViewType(position: Int): Int {
    return if (hasExtraRow() && position == itemCount - 1) {
      R.layout.network_state_item
    } else {
      R.layout.item_delivery_layout
    }
  }



  override fun getItemCount(): Int {
    return super.getItemCount() + if (hasExtraRow()) 1 else 0
  }


  fun setNetworkState(newNetworkState: NetworkState?) {
    val previousState = this.networkState
    val hadExtraRow = hasExtraRow()
    this.networkState = newNetworkState
    val hasExtraRow = hasExtraRow()
    if (hadExtraRow != hasExtraRow) {
      if (hadExtraRow) {
        notifyItemRemoved(super.getItemCount())
      } else {
        notifyItemInserted(super.getItemCount())
      }
    } else if (hasExtraRow && previousState != newNetworkState) {
      notifyItemChanged(itemCount - 1)
    }
  }

}