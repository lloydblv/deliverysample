package com.company.android.sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.snackbar.BaseTransientBottomBar.BaseCallback
import kotlinx.android.synthetic.main.activity_main.main_activity_root


class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    setTheme(R.style.AppTheme)
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val navController = findNavController(R.id.nav_host_fragment)
    setupActionBarWithNavController(navController)
  }

  override fun onSupportNavigateUp(): Boolean = findNavController(R.id.nav_host_fragment).navigateUp()

  var isBackPressedOnce = false
  override fun onBackPressed() {
    val navController = findNavController(R.id.nav_host_fragment)

    if (isBackPressedOnce || navController.currentDestination?.id == R.id.delivery_detail_fragment) {
      super.onBackPressed()
    } else {
      com.google.android.material.snackbar.Snackbar.make(main_activity_root,
          R.string.double_press_to_exit, com.google.android.material.snackbar.Snackbar.LENGTH_SHORT)
          .setAction(R.string.exit) {
            finish()
          }
          .addCallback(object : BaseCallback<com.google.android.material.snackbar.Snackbar>() {
            override fun onShown(
                transientBottomBar: com.google.android.material.snackbar.Snackbar?) {
              super.onShown(transientBottomBar)
              isBackPressedOnce = true
            }

            override fun onDismissed(
                transientBottomBar: com.google.android.material.snackbar.Snackbar?, event: Int) {
              super.onDismissed(transientBottomBar, event)
              isBackPressedOnce = false
            }
          })
          .show()
    }
  }
}
