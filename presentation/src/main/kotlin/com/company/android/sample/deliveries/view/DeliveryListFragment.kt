package com.company.android.sample.deliveries.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.company.android.sample.R
import com.company.android.sample.commons.BaseFragment
import com.company.android.sample.config.AppConfig
import com.company.android.sample.deliveries.adapter.DeliveryListAdapter
import com.company.android.sample.deliveries.domain.DeliveryListVMFactory
import com.company.android.sample.deliveries.presenter.DeliveryListViewModel
import ir.zinutech.android.data.entities.LocationData
import ir.zinutech.android.domain.entities.NetworkState
import kotlinx.android.synthetic.main.fragment_delivery_list_layout.delivery_list_fragment_rv
import kotlinx.android.synthetic.main.fragment_delivery_list_layout.delivery_list_fragment_swipe
import javax.inject.Inject

class DeliveryListFragment : BaseFragment() {

  @Inject
  lateinit var factoryList: DeliveryListVMFactory
  private lateinit var viewModel: DeliveryListViewModel


  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
      savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_delivery_list_layout,
      container, false)

  override fun onAttach(context: Context?) {
    super.onAttach(context)
    (activity?.application as AppConfig).createDeliveryListComponent().inject(this)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    viewModel = ViewModelProviders.of(this, factoryList).get(DeliveryListViewModel::class.java)
    if (savedInstanceState == null) {
      viewModel.getDeliveries()
    }
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    viewModel.deliveries.observe(this, Observer {
      (delivery_list_fragment_rv.adapter as DeliveryListAdapter).submitList(it)
    })

    viewModel.networkState.observe(this, Observer {
      (delivery_list_fragment_rv.adapter as DeliveryListAdapter).setNetworkState(it)
    })
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    delivery_list_fragment_swipe.setOnRefreshListener {
      viewModel.refresh()
    }

    viewModel.refreshState.observe(this, Observer {
      delivery_list_fragment_swipe.isRefreshing = it == NetworkState.LOADING

    })

    delivery_list_fragment_rv.adapter = DeliveryListAdapter(Glide.with(this@DeliveryListFragment), {
      viewModel.retry()

    }) { itemView, _ ->
      (delivery_list_fragment_rv.adapter as DeliveryListAdapter).getItem(itemView,
          delivery_list_fragment_rv)?.let { deliveryEntity ->


        val navOptions = NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_right)
            .setExitAnim(R.anim.slide_out_left)
            .setPopEnterAnim(R.anim.slide_in_left)
            .setPopExitAnim(R.anim.slide_out_right)
            .build()

        itemView.findNavController().navigate(
            DeliveryListFragmentDirections.actionDeliveryListToDetail(
                "${deliveryEntity.id}",
                deliveryEntity.imageUrl,
                deliveryEntity.description,
                LocationData(deliveryEntity.location.latitude, deliveryEntity.location.longitude, deliveryEntity.location.address)
            ), navOptions)
      }
    }
  }

  override fun onDestroyView() {
    super.onDestroyView()
    (activity?.application as AppConfig).releaseDeliveryListComponent()
  }
}