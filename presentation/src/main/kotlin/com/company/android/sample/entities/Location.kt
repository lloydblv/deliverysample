package com.company.android.sample.entities

import android.os.Parcel
import android.os.Parcelable

data class Location(
    val latitude: Double,
    val longitude: Double,
    val address: String
) : Parcelable {
  constructor(source: Parcel) : this(
      source.readDouble(),
      source.readDouble(),
      source.readString()
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeDouble(latitude)
    writeDouble(longitude)
    writeString(address)
  }

  companion object {
    @JvmField
    val CREATOR: Parcelable.Creator<Location> = object : Parcelable.Creator<Location> {
      override fun createFromParcel(source: Parcel): Location = Location(source)
      override fun newArray(size: Int): Array<Location?> = arrayOfNulls(size)
    }
  }
}