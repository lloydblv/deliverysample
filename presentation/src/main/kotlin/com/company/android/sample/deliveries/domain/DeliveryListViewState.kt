package com.company.android.sample.deliveries.domain

import com.company.android.sample.entities.Delivery


data class DeliveryListViewState(
    var isLoading: Boolean = true,
    var deliveries: List<Delivery>? = null,
    var pageDeliveries: List<Delivery>? = null
)