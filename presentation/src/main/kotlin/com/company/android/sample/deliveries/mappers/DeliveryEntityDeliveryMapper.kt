package com.company.android.sample.deliveries.mappers

import com.company.android.sample.entities.Delivery
import com.company.android.sample.entities.Location
import ir.zinutech.android.domain.common.Mapper
import ir.zinutech.android.domain.entities.DeliveryEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeliveryEntityDeliveryMapper @Inject constructor() : Mapper<DeliveryEntity, Delivery>() {
  override fun mapFrom(from: DeliveryEntity): Delivery {
    return Delivery(
        from.id,
        from.description,
        from.imageUrl,
        Location(from.location.latitude, from.location.latitude, from.location.address)
    )
  }
}