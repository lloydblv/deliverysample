package com.company.android.sample.details.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.company.android.sample.R
import com.company.android.sample.commons.BaseFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_delivery_details_layout.delivery_detail_fragment_header_tv
import kotlinx.android.synthetic.main.fragment_delivery_details_layout.delivery_detail_fragment_mapview
import kotlinx.android.synthetic.main.fragment_delivery_details_layout.view.delivery_detail_fragment_mapview

class DeliveryDetailFragment : BaseFragment() {

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
      savedInstanceState: Bundle?): View? = inflater.inflate(
      R.layout.fragment_delivery_details_layout,
      container, false).also {
    it.delivery_detail_fragment_mapview.onCreate(savedInstanceState)
  }


  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val description = DeliveryDetailFragmentArgs.fromBundle(arguments).description
    val location = DeliveryDetailFragmentArgs.fromBundle(arguments).location
    delivery_detail_fragment_header_tv.text = description


    delivery_detail_fragment_mapview.getMapAsync { googleMap ->
      val deliveryPosition = LatLng(location.lat, location.lng)
      googleMap.moveCamera(
          CameraUpdateFactory.newLatLngZoom(deliveryPosition, 1f))

      googleMap.addMarker(MarkerOptions().position(deliveryPosition)
          .snippet(location.address)
          .title(description))

    }
  }


  override fun onResume() {
    super.onResume()
    delivery_detail_fragment_mapview?.onResume()

  }

  override fun onDestroy() {
    super.onDestroy()
    delivery_detail_fragment_mapview?.onDestroy()

  }

  override fun onLowMemory() {
    super.onLowMemory()
    delivery_detail_fragment_mapview?.onLowMemory()

  }

}