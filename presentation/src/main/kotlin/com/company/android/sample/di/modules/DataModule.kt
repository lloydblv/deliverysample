package com.company.android.sample.di.modules

import android.content.Context
import ir.zinutech.android.data.repositories.DbDeliveryDataRepository
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import ir.zinutech.android.data.api.Api
import ir.zinutech.android.data.db.DeliveriesDb
import ir.zinutech.android.data.mappers.DeliveryDataEntityMapper
import ir.zinutech.android.data.repositories.DeliveryDataStore
import ir.zinutech.android.data.repositories.DeliveryRepositoryImpl
import ir.zinutech.android.domain.DeliveryRepository
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton


@Module
class DataModule {
  @Singleton
  @Provides
  fun provideCarsRepository(api: Api): DeliveryRepository = DeliveryRepositoryImpl(
      DeliveryDataStore(api))


  @Singleton
  @Provides
  fun provideDeliveryDatabase(context: Context): DeliveriesDb = DeliveriesDb.create(context)


  @Singleton
  @Provides
  fun provideIOExecutor(): Executor = Executors.newSingleThreadExecutor()

  @Provides
  fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

  @Singleton
  @Provides
  fun provideDeliveryRepository(deliveriesDb: DeliveriesDb, api: Api,
      compositeDisposable: CompositeDisposable,
      executor: Executor,
      deliveryDataEntityMapper: DeliveryDataEntityMapper): ir.zinutech.android.data.repositories.DeliveryRepository = DbDeliveryDataRepository(
      deliveriesDb,
      api,
      compositeDisposable,
      executor,
      20,
      deliveryDataEntityMapper

  )
}