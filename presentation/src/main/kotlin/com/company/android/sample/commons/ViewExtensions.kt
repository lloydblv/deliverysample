package com.company.android.sample.commons

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes


fun View.toVisible() {
  visibility = View.VISIBLE
}

fun View.toGone() {
  visibility = View.GONE
}

typealias RecyclerViewClickListener = (View, Int) -> (Unit)


fun ViewGroup.inflate(@LayoutRes layoutRes: Int, isAttachToRoot: Boolean = false): View {
  return LayoutInflater.from(context).inflate(layoutRes, this, isAttachToRoot)
}