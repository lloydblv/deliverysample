package com.company.android.sample.deliveries.injection

import com.company.android.sample.deliveries.view.DeliveryListFragment
import dagger.Subcomponent


@Subcomponent(modules = [DeliveryListModule::class])
interface DeliveryListSubComponent {
  fun inject(deliveryListFragment: DeliveryListFragment)
}