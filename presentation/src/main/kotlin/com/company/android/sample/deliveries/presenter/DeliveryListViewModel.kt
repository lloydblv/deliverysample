package com.company.android.sample.deliveries.presenter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import com.company.android.sample.commons.BaseViewModel
import com.company.android.sample.commons.SingleLiveEvent
import com.company.android.sample.deliveries.domain.DeliveryListViewState
import ir.zinutech.android.data.entities.Listing
import ir.zinutech.android.data.repositories.DeliveryRepository
import ir.zinutech.android.domain.entities.DeliveryEntity
import timber.log.Timber

class DeliveryListViewModel(
    private val repository: DeliveryRepository) : BaseViewModel() {


  private val repoResult = MutableLiveData<Listing<DeliveryEntity>>()


  val deliveries = switchMap(repoResult) { it.pagedList }!!
  val networkState = switchMap(repoResult) { it.networkState }!!
  val refreshState = switchMap(repoResult) { it.refreshState }!!


  fun refresh() {
    repoResult.value?.refresh?.invoke()
  }

  fun retry() {
    val listing = repoResult.value
    listing?.retry?.invoke()
  }

  var listViewState: MutableLiveData<DeliveryListViewState> = MutableLiveData()
  var errorState: SingleLiveEvent<Throwable?> = SingleLiveEvent()

  private var pageIndex: MutableLiveData<Int> = MutableLiveData()

  init {
    listViewState.value = DeliveryListViewState()
    pageIndex.value = 1
  }

  fun getDeliveries() {
    Timber.d("getDeliveries()")
    repoResult.value = repository.deliveryList(20)
  }
}