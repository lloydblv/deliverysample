package com.company.android.sample.deliveries.domain

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.company.android.sample.deliveries.presenter.DeliveryListViewModel
import ir.zinutech.android.data.repositories.DeliveryRepository

class DeliveryListVMFactory(
    private val repository: DeliveryRepository
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    return DeliveryListViewModel(repository) as T
  }

}