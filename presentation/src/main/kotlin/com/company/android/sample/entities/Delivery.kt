package com.company.android.sample.entities


data class Delivery(
    val id: Int,
    var description: String,
    var imageUrl: String,
    var location: Location
)