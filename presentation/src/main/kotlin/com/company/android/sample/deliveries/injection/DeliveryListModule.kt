package com.company.android.sample.deliveries.injection

import com.company.android.sample.commons.AsyncTransformer
import com.company.android.sample.deliveries.domain.DeliveryListVMFactory
import dagger.Module
import dagger.Provides
import ir.zinutech.android.domain.DeliveryRepository
import ir.zinutech.android.domain.usecases.GetDeliveries

//@DeliveryListScope
@Module
class DeliveryListModule {

  @Provides
  fun provideDeliveryListUseCase(deliveryRepository: DeliveryRepository) = GetDeliveries(
      AsyncTransformer(), deliveryRepository)

  @Provides
  fun provideDeliveryListVMFactory(
      deliveryRepository: ir.zinutech.android.data.repositories.DeliveryRepository) = DeliveryListVMFactory(
      deliveryRepository)

}