package com.company.android.sample.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import ir.zinutech.android.data.api.Api
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BASIC
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class NetModule {

  companion object {
    private const val BASE_URL = "https://mock-api-mobile.dev.lalamove.com/"
  }

  @Singleton
  @Provides
  fun provideInterceptors(): ArrayList<Interceptor> {
    val interceptors = arrayListOf<Interceptor>()
    interceptors.add(HttpLoggingInterceptor().setLevel(BASIC))
//    interceptors.add(HttpLoggingInterceptor().setLevel(BODY))
    return interceptors
  }

  @Provides
  @Singleton
  fun providesGson(): Gson = GsonBuilder().create()

  @Singleton
  @Provides
  fun provideRetrofit(gson: Gson, interceptors: ArrayList<Interceptor>): Retrofit{
    val okHttpBuilder = OkHttpClient.Builder()
    if (interceptors.isNotEmpty()) {
      interceptors.forEach {
        okHttpBuilder.addInterceptor(it)
      }
    }

    return Retrofit.Builder()
        .client(okHttpBuilder.build())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .baseUrl(BASE_URL)
        .build()
  }

  @Singleton
  @Provides
  fun provideApi(retrofit: Retrofit): Api = retrofit.create(Api::class.java)
}