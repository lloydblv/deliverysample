package com.company.android.sample.deliveries.injection

import javax.inject.Scope
import kotlin.annotation.AnnotationRetention.RUNTIME

@Scope
@Retention(RUNTIME)
annotation class DeliveryListScope