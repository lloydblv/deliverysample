package com.company.android.sample.config

import android.app.Application
import com.company.android.sample.BuildConfig
import com.company.android.sample.di.components.DaggerMainComponent
import com.company.android.sample.di.components.MainComponent
import com.company.android.sample.di.modules.ContextModule
import com.company.android.sample.di.modules.DataModule
import com.company.android.sample.di.modules.NetModule
import com.company.android.sample.deliveries.injection.DeliveryListModule
import com.company.android.sample.deliveries.injection.DeliveryListSubComponent
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber
import timber.log.Timber.DebugTree

class AppConfig : Application() {

  private lateinit var mainComponent: MainComponent
  private var deliveryListComponent: DeliveryListSubComponent? = null
  override fun onCreate() {
    super.onCreate()


    if (BuildConfig.DEBUG) {
      Timber.plant(DebugTree())
      if(LeakCanary.isInAnalyzerProcess(this)) return
      LeakCanary.install(this)
    }

    mainComponent = DaggerMainComponent.builder()
        .contextModule(ContextModule(this))
        .netModule(NetModule())
        .dataModule(DataModule())
        .build()

  }

  fun createDeliveryListComponent(): DeliveryListSubComponent {
    deliveryListComponent = mainComponent.plus(DeliveryListModule())
    return deliveryListComponent!!
  }

  fun releaseDeliveryListComponent() {
    deliveryListComponent = null
  }

}