/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.company.android.sample.deliveries.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.company.android.sample.R
import ir.zinutech.android.domain.entities.DeliveryEntity

/**
 * A RecyclerView ViewHolder that displays a reddit deliveryData.
 */
class DeliveryViewHolder(view: View, private val glide: RequestManager)
  : RecyclerView.ViewHolder(view) {

  private val title: TextView = view.findViewById(R.id.title)
  private val subtitle: TextView = view.findViewById(R.id.subtitle)
  private val score: TextView = view.findViewById(R.id.score)
  private val thumbnail: ImageView = view.findViewById(R.id.thumbnail)
  private var deliveryData: DeliveryEntity? = null

  fun bind(post: DeliveryEntity?) {
    this.deliveryData = post
    title.text = post?.description ?: "loading"
    subtitle.text = post?.location?.address ?: "loading address"
    score.text = "${post?.id ?: 0}"
    if (post?.imageUrl?.startsWith("http") == true) {
      thumbnail.visibility = View.VISIBLE
      glide.load(post.imageUrl)
          .apply(RequestOptions.centerCropTransform().placeholder(R.mipmap.ic_launcher))
          .into(thumbnail)
    } else {
      thumbnail.visibility = View.GONE
      glide.clear(thumbnail)
    }
  }

  companion object {
    fun create(parent: ViewGroup, glide: RequestManager): DeliveryViewHolder {
      val view = LayoutInflater.from(parent.context).inflate(R.layout.item_delivery_layout, parent,
          false)
      return DeliveryViewHolder(view, glide)
    }
  }

  fun updateScore(item: DeliveryEntity?) {
    deliveryData = item
    score.text = "${item?.id ?: 0}"
  }
}