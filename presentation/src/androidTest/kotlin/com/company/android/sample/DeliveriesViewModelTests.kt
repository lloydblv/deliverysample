package com.company.android.sample

import android.test.UiThreadTest
import androidx.lifecycle.Observer
import androidx.test.runner.AndroidJUnit4
import com.company.android.sample.deliveries.domain.DeliveryListViewState
import com.company.android.sample.deliveries.mappers.DeliveryEntityDeliveryMapper
import com.company.android.sample.deliveries.presenter.DeliveryListViewModel
import io.reactivex.Single
import ir.zinutech.android.domain.DeliveryRepository
import ir.zinutech.android.domain.common.DomainTestUtils
import ir.zinutech.android.domain.common.TestTransformer
import ir.zinutech.android.domain.usecases.GetDeliveries
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyZeroInteractions


@Suppress("UNCHECKED_CAST")
@RunWith(AndroidJUnit4::class)
class DeliveriesViewModelTests {

  private lateinit var deliveryListViewModel: DeliveryListViewModel
  private lateinit var deliveryRepository : DeliveryRepository

  private lateinit var listViewObserver: Observer<DeliveryListViewState>
  private lateinit var errorObserver: Observer<Throwable?>

  private val deliveryEntityDeliveryMapper = DeliveryEntityDeliveryMapper()

  @Before
  @UiThreadTest
  fun before(){
    deliveryRepository = mock(DeliveryRepository::class.java)
    val getDeliveries = GetDeliveries(TestTransformer(), deliveryRepository)

    deliveryListViewModel = DeliveryListViewModel(getDeliveries, deliveryEntityDeliveryMapper)

    listViewObserver = mock(Observer::class.java) as Observer<DeliveryListViewState>
    errorObserver = mock(Observer::class.java) as Observer<Throwable?>

    deliveryListViewModel.listViewState.observeForever(listViewObserver)
    deliveryListViewModel.errorState.observeForever(errorObserver)
  }

  @Test
  @UiThreadTest
  fun testInitialViewStateShowsLoading(){
    verify(listViewObserver).onChanged(DeliveryListViewState(isLoading = true, deliveries = null, pageDeliveries = null))
    verifyZeroInteractions(listViewObserver)
  }


  @Test
  @UiThreadTest
  fun testShowingDeliveriesAsExpectedAndStopsLoading(){

    val testDeliveryEntitiesList = DomainTestUtils.generateDeliveryEntityList()

    `when`(deliveryRepository.getDeliveryList(1)).thenReturn(Single.just(testDeliveryEntitiesList))

    deliveryListViewModel.getDeliveries()

    val deliveries = testDeliveryEntitiesList.map { deliveryEntityDeliveryMapper.mapFrom(it) }
    verify(listViewObserver).onChanged(DeliveryListViewState(isLoading = false, pageDeliveries = deliveries, deliveries = deliveries))
    verify(errorObserver).onChanged(null)
  }


  @Test
  @UiThreadTest
  fun testShowingErrorMessageWhenNeeded(){
    val throwable = Throwable("ERROR!")
    `when`(deliveryRepository.getDeliveryList(1)).thenReturn(Single.error(throwable))
    deliveryListViewModel.getDeliveries()

    verify(listViewObserver).onChanged(DeliveryListViewState(isLoading = false, pageDeliveries = null))
    verify(errorObserver).onChanged(throwable)
  }

}