package ir.zinutech.android.domain.entities


data class DeliveryEntity(
    val id: Int,
    var description: String,
    var imageUrl: String,
    var location: LocationEntity
)