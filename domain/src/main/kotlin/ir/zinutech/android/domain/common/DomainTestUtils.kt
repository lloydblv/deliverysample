package ir.zinutech.android.domain.common

import ir.zinutech.android.domain.entities.DeliveryEntity
import ir.zinutech.android.domain.entities.LocationEntity


object DomainTestUtils {
  fun getTestDeliveryEntity(id: Int): DeliveryEntity =
      DeliveryEntity(id, "description_$id", "imageUrl_$id", getLocationEntity())

  fun getLocationEntity() = LocationEntity(1.0, 2.0, "Address")

  fun generateDeliveryEntityList(): List<DeliveryEntity> =
      (0..5).map { getTestDeliveryEntity(it) }
  
}