package ir.zinutech.android.domain.usecases

import io.reactivex.Single
import ir.zinutech.android.domain.DeliveryRepository
import ir.zinutech.android.domain.common.Transformer
import ir.zinutech.android.domain.entities.DeliveryEntity

class GetDeliveries(transformer: Transformer<List<DeliveryEntity>>,
    private val deliveryRepository: DeliveryRepository) : UseCase<List<DeliveryEntity>>(transformer) {

  companion object {
    private const val PARAM_PAGE_INDEX = "param:page_index"
  }

  fun get(pageIndex: Int, pageSize: Int): Single<List<DeliveryEntity>> = single(
      hashMapOf(PARAM_PAGE_INDEX to pageIndex))

  override fun createSingle(data: Map<String, Any>?): Single<List<DeliveryEntity>> = data?.get(
      PARAM_PAGE_INDEX)?.let { pageIndex ->
    deliveryRepository.getDeliveryList(pageIndex as Int)
  } ?: Single.just(emptyList())
}