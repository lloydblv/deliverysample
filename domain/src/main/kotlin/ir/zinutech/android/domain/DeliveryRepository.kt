package ir.zinutech.android.domain

import io.reactivex.Single
import ir.zinutech.android.domain.entities.DeliveryEntity

interface DeliveryRepository {
  fun getDeliveryList(pageIndex: Int = 1): Single<List<DeliveryEntity>>
}