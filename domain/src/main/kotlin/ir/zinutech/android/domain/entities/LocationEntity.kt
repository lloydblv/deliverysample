package ir.zinutech.android.domain.entities

data class LocationEntity(
    val latitude: Double,
    val longitude: Double,
    val address: String
)