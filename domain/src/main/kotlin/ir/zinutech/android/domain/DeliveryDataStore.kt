package ir.zinutech.android.domain

import io.reactivex.Single
import ir.zinutech.android.domain.entities.DeliveryEntity

interface DeliveryDataStore {
  fun getDeliveryList(pageIndex: Int = 0): Single<List<DeliveryEntity>>
}