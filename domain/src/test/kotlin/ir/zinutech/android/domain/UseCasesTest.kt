package ir.zinutech.android.domain

import io.reactivex.Single
import ir.zinutech.android.domain.common.DomainTestUtils
import ir.zinutech.android.domain.common.TestTransformer
import ir.zinutech.android.domain.usecases.GetDeliveries
import org.junit.Test
import org.mockito.Mockito

class UseCasesTest {
  @Test
  fun getDeliveryList() {

    val deliveryRepository = Mockito.mock(DeliveryRepository::class.java)

    Mockito.`when`(deliveryRepository.getDeliveryList(1)).thenReturn(
        Single.just(DomainTestUtils.generateDeliveryEntityList()))

    val getDeliveries = GetDeliveries(TestTransformer(), deliveryRepository)

    getDeliveries.get(1, 20).test()
        .assertValue { it.size == 6 }
        .assertComplete()
  }

  @Test
  fun getDeliveryListNoResultsReturnsEmpty() {
    val deliveryRepository = Mockito.mock(DeliveryRepository::class.java)

    Mockito.`when`(deliveryRepository.getDeliveryList(1)).thenReturn(Single.just(emptyList()))

    val getDeliveries = GetDeliveries(TestTransformer(), deliveryRepository)
    getDeliveries.get(0, 20).test()
        .assertValue { it.isEmpty() }
        .assertComplete()
  }
}